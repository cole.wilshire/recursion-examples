#include <iostream>
using namespace std;

int p = 0;	//p is the product's value, initialized to 0

void product(int a, int b)	//Method to calculate product recursively
{
	if ((b == 0) || (b == 1) || (p == 1) || (p == a)) {	//Base case
		return;
	}
	else
	{
		p = a + (a * (b - 1));	//General solution
		return;
	}
}

int main()
{
	int a = 7;	//Value of first number to be multiplied
	int b = 12;	//Value of second number to be multiplied
	product(a, b);	//Call product function
	cout << a << " * " << b << " = " << p << endl << endl;	//Print value of product to console
	return 0;
}
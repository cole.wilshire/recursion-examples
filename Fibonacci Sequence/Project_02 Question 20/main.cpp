#include <iostream>
using namespace std;

int fibonacci(int n) {
	if ((n == 1) || (n == 2)) {		//Base Case
		return(1);	//Return value of 1 if n = 1 or 2
	}
	else {
		return(fibonacci(n - 1) + fibonacci(n - 2));	//General Solution
	}
}
int main() {
	int n = 45;	//Highest term of series
	for (int i = 1; i < n; i++) {	//Output list of all Fibonacci numbers, term 1 through term 45
		cout << fibonacci(i) << endl;
	}
	return 0;	//End program
}